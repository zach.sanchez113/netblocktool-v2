import time
from typing import Optional

import lxml
import requests
from lxml import html

# To get around the user agent whitelist
HEADERS = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36",
}


class Company:
    def __init__(self, name, cik):
        self.name = name
        self.cik = cik

    def getAllFilings(
        self,
        filingType: str = "",
        priorTo: str = "",
        ownership: str = "include",
        noOfEntries: int = 100,
    ) -> lxml.html.HtmlElement:
        return getRequest(
            href="https://www.sec.gov/cgi-bin/browse-edgar",
            params={
                "action": "getcompany",
                "CIK": self.cik,
                "type": filingType,
                "dateb": priorTo,
                "owner": ownership,
                "count": noOfEntries,
            },
        )


class Edgar:
    def __init__(self):
        response = requests.request(
            method="GET",
            url="https://www.sec.gov/Archives/edgar/cik-lookup-data.txt",
            headers=HEADERS,
        )

        if not response.ok:
            raise Exception(
                f"{repr(response)} Failed to get CIK lookup data from EDGAR!"
                + " Visit 'https://www.sec.gov/Archives/edgar/cik-lookup-data.txt' to check if you've exceeded the rate limit."
            )

        # Because rate limits
        time.sleep(1)

        all_companies_array = response.content.decode("latin1").split("\n")[:-1]  # final is empty str

        all_companies_array_rev = []

        for i, item in enumerate(all_companies_array):
            if item == "":
                continue

            item_arr = item.split(":")

            # Create a tuple of the form (<company name>, <CIK>)
            all_companies_array[i] = (item_arr[0], item_arr[1])

            # Create a tuple of the form (<CIK>, <company name>)
            all_companies_array_rev.append((item_arr[1], item_arr[0]))

        self.all_companies_dict = dict(all_companies_array)
        self.all_companies_dict_rev = dict(all_companies_array_rev)

    def getCikByCompanyName(self, name):
        return self.all_companies_dict[name]

    def getCompanyNameByCik(self, cik):
        return self.all_companies_dict_rev[cik]

    def findCompanyName(self, words):
        possibleCompanies = []
        words = words.lower()
        for company in self.all_companies_dict:
            if all(word in company.lower() for word in words.split(" ")):
                possibleCompanies.append(company)
        return possibleCompanies


def getRequest(href: str, params: Optional[dict] = None) -> lxml.html.HtmlElement:
    response = requests.get(
        href,
        headers=HEADERS,
        params=params if params else {},
    )

    # Because rate limits suck
    time.sleep(1)

    return html.fromstring(response.content)


def getDocuments(tree, noOfDocuments=1) -> list:
    result = []

    elems = tree.xpath('//*[@id="documentsbutton"]')[:noOfDocuments]
    for elem in elems:
        elem_href = elem.attrib["href"]
        contentPage = getRequest(f"https://www.sec.gov{elem_href}")

        elem_href = contentPage.xpath('//*[@id="formDiv"]/div/table/tr[2]/td[3]/a')[0].attrib["href"]
        filing = getRequest(f"https://www.sec.gov{elem_href}")

        result.append(filing.body.text_content())

    if len(result) == 1:
        return result[0]

    return result
